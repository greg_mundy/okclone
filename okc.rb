require 'sinatra'
require 'sinatra/flash'
require 'sinatra/reloader' if development?
require 'json'

configure :development do
    enable :session
end

get '/' do
    erb :index, :layout => :template
end

get '/signin' do
    erb :signin, :layout => :template
end

get '/register' do
    erb :register, :layout => :template
end
